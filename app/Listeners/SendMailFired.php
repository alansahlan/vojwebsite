<?php

namespace App\Listeners;

use App\Events\SendMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendMailFired
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendMail  $event
     * @return void
     */
    public function handle(SendMail $event)
    {
        $command = $event->command;
        $data = $event->data;
        $mailview = (is_null($event->mailview) || $event->mailview == '')?'emails.generic':$event->mailview;

        $data['cc'] = isset($data['cc'])?$data['cc']:'';
        $data['bcc'] = isset($data['bcc'])?$data['bcc']:'';
        $data['subject'] = isset($data['subject'])?$data['subject']:'';
        $data['sender'] = isset($data['sender'])?$data['sender']:'';
        $data['sender_name'] = isset($data['sender_name'])?$data['sender_name']:'';
        $data['reply_to'] = isset($data['reply_to'])?$data['reply_to']:'';
        $data['reply_to_name'] = isset($data['reply_to_name'])?$data['reply_to_name']:'';
        $data['level'] = isset($data['level'])?$data['level']:'high';

        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);

        $beautymail->send($mailview,$data, function($message) use($data) {
            $message->to($data['to']);
            if($data['cc'] != ''){
                $message->cc($data['cc']);
            }
            if($data['bcc'] != ''){
                $message->bcc($data['bcc']);
            }
            $message->subject($data['subject']);

            $message->from(env('MAIL_FROM'), env('MAIL_FROM_NAME'));

            if($data['sender'] != ''){
                $message->sender($data['sender'], $data['sender_name']);
            }

            if($data['reply_to'] != ''){
                $message->replyTo($data['reply_to'], $data['reply_to_name']);
            }

            $message->priority($data['level']);
            if(isset($data['attachment'])){
                $att = $data['attachment'];
                $options = isset($att['options'])?$att['options']:[];//must be array
                //filepaths also must be array
                foreach($att['filepaths'] as $filepath){
                    $message->attach($filepath, $options);
                }
            }
        });
        //
    }
}
