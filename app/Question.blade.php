<?php
namespace App\Models;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Question extends Eloquent {

    protected $collection = 'question';

}