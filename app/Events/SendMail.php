<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendMail extends Event
{
    use SerializesModels;

    public $command;
    public $data;
    public $mailview;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($command,$data,$mailview)
    {
        //
        $this->command = $command;
        $this->data = $data;
        $this->mailview = $mailview;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
