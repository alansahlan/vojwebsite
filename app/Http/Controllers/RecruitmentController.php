<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Helpers\Prefs;

use Creitive\Breadcrumbs\Breadcrumbs;

use App\Models\Question;

use App\Models\Pertanyaan;

use Auth;
use Event;
use View;
use Input;
use Response;
use Mongomodel;
use \MongoRegex;
use DB;
use HTML;

class RecruitmentController extends Controller
{
        public function getForm(){
    	return view('recruitment.formregister');
    }

    public function postForm(Request $in){

    	$model = new Question();

    	$in = $in->all();

    	foreach($in as $k=>$v){
    		if($k != ''){
    			$model->$k = ($v == '')?'-':$v;
    		}
    	}
    	$model->save();
    	return view('recruitment.post');
    }
    public function getData(){

 		$data = 'question';
 		$list_data = Question::all();
 		return view('recruitment.getdata',compact('data','list_data'));
 	}
    public function show(){
        return view('recruitment');
    }

}
