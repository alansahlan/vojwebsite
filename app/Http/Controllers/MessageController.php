<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\SendMail;

use App\Http\Requests;

use Response;
use Mongomodel;
use Event;
use App\Mail;
use View;
use Session;

class MessageController extends Controller
{
    public function postSend(Request $request){
    	$model = New Mail();

    	$model->name = $request['name'];
    	$model->email = $request['email']; 
    	$model->subject = $request['subject']; 
    	$model->pesan = $request['pesan']; 	

    	$model->save();

    	$data = array(
    			'to'=> 'halo@voiceofjakarta.com',
                'bcc' => 'inggita@gmail.com',
    			'pesan' => $request['pesan'],
    			'subject' => $request['subject'],
    			'email' => $request['email'],
    			'name' => $request['name']
    			
    		);
    	
    	
    	Event::fire(new SendMail('sendmail', $data, 'emails.send'));
    	Session::flash('flash_message','Email anda telah berhasil dikirim.');

    	return redirect('/');
    }
}
