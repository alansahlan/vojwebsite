<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', function () {
    return view('index');
});

Route::get('about',function(){
	return view('pages.about');
});
Route::get('pages/{s}',function($s){
	return view('pages.'.$s);
});
*/

Route::get('/',function(){
     return view('home');
});

Route::get('talent','RecruitmentController@getForm');

Route::post('contactus', 'MessageController@postSend');

Route::get('testmail', function(){
	$beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
	$beautymail->send('emails.welcome',[], function($message){
		$message->to('alan.alenyor@gmail.com')->subject('Test Email');
	});
});