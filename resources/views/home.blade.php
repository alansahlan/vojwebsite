<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Voice Of Jakarta</title>
    <link rel="shortcut icon" href="{{ url('images')}}/logoblue.png" style="border-radius:30px;">
    <link href='https://fonts.googleapis.com/css?family=Raleway:500,600,700,800,900,400,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
    <link  rel="stylesheet" href="{{URL('asset') }}/css/bootstrap.min.css " >
    <link rel="stylesheet"  href="{{URL('asset') }}/css/owl.carousel.css " >
    <link  rel="stylesheet" href="{{URL('asset') }}/css/owl.theme.css " >
    <link rel="stylesheet" href="{{URL('asset') }}/css/Pe-icon-7-stroke.css ">
    <link rel="stylesheet" href="{{URL('asset') }}/css/font-awesome.min.css " >
    <link  rel="stylesheet" href="{{URL('asset') }}/css/prettyPhoto.css " >
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <link rel="stylesheet"  href="{{URL('asset') }}/css/style.css " >
    <link  rel="stylesheet" href="{{URL('asset') }}/css/animate.css " >
    <link  rel="stylesheet" href="{{URL('asset') }}/css/responsive.css " >
    
    <style type="text/css">
        @media screen and (max-width: 480px){
            img.logos{
                height: 60px !important;
                width: auto;
                padding-left: 20px;
                }
                .col-md-3{
                padding-left:6px !important;
                width: auto !important;
                }
                .col-md-4{
                    padding-left:0px !important;
                }
                .col-md-2{
                    padding-left:8px !important;
                }
                .section_title{
                    padding-top: 10px !important;
                }
                .section_overlay{
                    padding-left: 15px !important;
                }
                .test{
                padding-bottom: 30px !important;
            }
                .section_title{
            
                padding-left:25px !important;
            }
                .col-md-3{
                    
                }
        }
        @media screen and(min-width: 780px){
            .col-md-3{
                    width: 100% !important;
                }
        }

        @media screen and (max-width: 870px){
            img.logos{
                height: 90px !important;
                width: auto;
                padding-left: 20px;
                }
                .col-md-3{
                padding-left:6px !important;
                width: auto !important;
                }
                .col-md-4{
                    padding-left:0px !important;
                }
                .col-md-2{
                    padding-left:8px !important;
                }
                .section_title{
                    padding-top: 20px !important;
                }
                .section_overlay{
                    padding-left: 30px !important;
                }
                
                .section_title{
            
                    padding-left:30px !important;
                }
                .test{
                    padding-bottom: 10px !important;
                }
                
                h3.text{
                    color: white;
                    padding-top: 18px !important;
                    text-shadow: 2px 2px #050202;
                    line-height: 10px;
                    padding-left: 15px;
                }
                h3{
                    padding-left: 10px;
                }    
            }

            @media screen and (min-width: 992px){
                img.logos{
                    height: 120px !important;
                    width: auto;
                    padding-left: 10px;
                }
                
                h3.text{
                    font-size: 14px !important;
                    color: white;
                    padding-top: 2px;
                    text-shadow: 2px 2px #050202;
                    line-height: 20px;
                    margin-top: 10px !important;
                    }
                h3.device{
                    font-size: 17px !important;
                    color: white;
                    padding-top: 2px !important;
                    text-shadow: 2px 2px #050202;
                    line-height: 19px;
                    margin-top: 10px !important;
                    }
                h3{
                    font-size: 18px ;
                    color: white;
                    padding-top: 10px !important; 
                    text-shadow: 2px 2px #050202;
                    line-height: 23px;
                }
                .col-md-3{
                    width: 30.5% !important;
                }
                 }

        /*.test{
            padding-bottom: 100px;
        }*/

        img.img-center{
            max-height: 70px;
        }
        .section_title{
            padding-top: 30px !important;
            padding-bottom: 20px;
            padding-left:45px;
        }
        .btnhow{
            border: 0px solid;
            height:50px; width:175px; 
            background-color:#428bca;
            border-radius: 35px; 
            font-size:17px;
        }
        .a-nav{
            color:#428bca !important; font-size:18px !important;
        }
        .fun_facts {
            background: url({{'images/benchnew.jpg'}}) no-repeat center;
            background-size: cover;
        }
        h3.text{
            font-size: 18px;
            color: white;
            padding-top: 2px;
            text-shadow: 2px 2px #050202;
            line-height: 20px;
        }
        h3.device{
            
            margin-top: 20px !important;
            }
        h3{
            font-size: 20px ;
            color: white;
            padding-top: 20px; 
            text-shadow: 2px 2px #050202;
            line-height: 27px;
        }

        
        


        div#line1 span#a{
            display: inline;
        }
        div#line1:hover span#a {
          display: none;
        }

        div#line1 span#b {
          display: none;
        }

        div#line1:hover span#b {
          display: inline;
        }


    </style>
</head>
<body>

    <div class="spn_hol">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <section class="header parallax home-parallax page" id="HOME" style="background: url({{'images/balkonnew.jpg'}})!important;background-size: cover !important;
    background-repeat: no-repeat !important; height:700px !important;">
        <h2></h2>
        <div class="section_overlay">
            <nav class="navbar-default navbar-fixed-top" role="" style="background-color: rgba(0, 0, 0, 0.7) !important;">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbars" href="#">
                            <img class="logos" src="{{URL('images/logofix.png')}}" alt="Logo" class="" style="height:170px ">
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a class="a-nav" href="#HOME">HOME</a> </li>
                            <li><a class="a-nav" href="#ABOUT">ABOUT </a> </li>
                            <li><a class="a-nav" href="#FEATURES">PROJECTS</a></li>
                            <li><a class="a-nav" style="border-color:#f0ad4e; border: 1px solid; " href="#CONTACT">WORK WITH US</a> </li>
                        </ul>
                    </div> 
                </div>
            </nav>

            <div class="container home-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo text-center">
                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <div class="home" style="color:white;">
                            <h1 style="font-size:18px;">HOW TO WIN WITH DIGITAL</h1>
                            <p>business and performance management solution </p>
                            <p> for companies and institutions</p>
                            <br>
                            <button class="btnhow" type="button" class="btn btn-secondary" style=""><a href="#FEATURES" style="color: white;"> Find out how</a></button>
                            <p></p>
                            <br>
                            <p> Web and Mobile App * Platform/system</p>
                            <p> Digital services * Digital media</p>
                        </div>
                        @include('_partial.flash_message')
                    </div>
                    <div class="col-md-3 col-md-offset-1 col-sm-4" >
                        <div class="home-iphone" >
                            
                        </div>
                         <span id="ABOUT"></span>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <div class="test" ></div>
    <!-- END HEADER SECTION -->
 <!-- =========================
     START ABOUT US SECTION
============================== -->
    <section class="about page"  style="padding-top: 35px !important;">
        <div class="container">
            
                <div class="row">
                    <div class="col-md-10 ">          
                        <div class="section_title"> 
                            <img style=" max-height:100px; position:center;" class="" src="{{ url('images')}}/logo.png">
                            <p style="padding-bottom: 5px !important; text-align: left;">VOJ creates web-based and mobile applications and platforms to help companies and institutions manage logistics, human resources, and other aspects of their operations.  VOJ builds and operates digital services for e-commerce, hospitality industry, IT and fleet management: halofix, halorides. VOJ’s lifestyle platform, TripSista, not only has its finger on the pulse of the fast-growing travel industry, but also empowers women and communities to participate in it.</p><p style="margin-top: 5px !important; text-align: left;">
                            VOJ started out as a New York City-based music streaming, Voice of Jakarta, born straight into the digital age in 2004. It was incorporated and consolidated in Indonesia in 2016, creating digital solution to improve operations of businesses, or to complement brand & reputation initiatives in a world where influence is built on digital media. </p>
                        </div>
                    </div>
                </div>
            
        <div class="inner_about_area" >
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="about_phone wow fadeInLeft"> 
                        </div>
                   
                    <div class="col-md-9  wow fadeInRight">
                        
                        <div class="inner_about_title" >
                           
                        <div class="inner_about_desc">

                            
                            <!-- <div class="single_about_area fadeInUp wow">
                                <div><a href="http://halofix.co/" target="blank">
                                    <img class="img-center" src="{{ url('images')}}/halofix.png">
                                </a></div>
                                <br/>
                                <p>Affordable and reliable IT helpdesk service for businesses, institutions, and individuals - taking care of digital needs. 
                                Find out more at <a target="blank" href="http://halofix.co/"> halofix.co</a>  (currently available in Jakarta area only)</p>
                            </div>
                                <br><br> -->
                            <div class="single_about_area fadeInUp wow">
                               
                                <div>
                                <a href="http://halorides.com/" target="blank"><img class="img-center" src="{{ url('images')}}/halorides.png"></a> </div>
                                <br>
                                <p>Helping hotels and hospitality industry fulfil their guests’ needs by managing requests all the way to the delivery of service by ground staffs. An indispensable system to improve customer satisfaction without adding more cost to the operations. Request for information on halorides<a href="#CONTACT"> today</a>. (currently available in Jakarta and Tangerang only).</p>
                            </div>
                                <br><br>
                            <div class="single_about_area fadeInUp wow" >
                                
                                <div>
                                    <a href="http://tripsista.com/" target="blank">
                                       <img class="img-center" style="max-height:50px;" src="{{ url('images')}}/ts-logo-dark.png"> 
                                    </a>
                                </div>
                                <br>
                               
                                <p>An online social platform for travelers where members post, share, or mark the destinations they want to visit to find travelmates, also the online tool to organize trips, share their stories, and buy/sell travel-related goods via this marketplace. Sign up at <a id="FEATURES" href="http://tripsista.com" target="blank">TripSista.com</a> and start working on your travel wishlist.</p>
                            </div>
                            
                        </div>
                    </div>
                </div>
                 </div>
            </div>
        </div>
    </div>
</section>
<div class="test" ></div>
<section class=" page" > 
    <div class="container">
        <div class="inner_about_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s"> 
                        </div>
                   
                        <div class="col-md-10  wow fadeInRight" data-wow-duration="1s" data-wow-delay=".5s">
                            
                            <div class="inner_about_title" >
                                <h2  style="text-transform:none !important;">Projects</h2>
                                <p>Please move your cursor over these projects and see how we can help your businesses grow.</p>
                                <p></p>
                                <div class="inner_about_desc">
                                    <div class="row">
                                    <div class="col-md-12">
                                            <div class="col-md-3" id="line1" style="background: url({{'images/projects/jaelynn-castillo.jpg'}})!important;background-size: cover !important; 
                                                background-repeat: no-repeat !important; 
                                                height:210px !important;">
                                                <span id="a"> <h3>Mobile app to engage and update readers and members. (Android and iOS)</h3></span>
                                                <span id="b"><h3 class="device" style="line-height: 20px; font-size: 20px;">For online media, reader engagement is everything.  Our mobile app helps content producers and companies engage their audience and customers.</h3></span>
                                            </div>
                                            <div class="col-md-1" style="width:1% !important;"></div>
                                            <div class="col-md-3" id="line1" style="background: url({{'images/projects/antonio.jpg'}})!important;background-size: cover !important;
                                         background-repeat: no-repeat !important; height:210px !important; ">  
                                                <span id="a"> <h3>Last-mile courier app to control the quality of delivery and improve customer satisfaction</h3></span><span id="b"><h3 class="text">Online marketplaces rely heavily on logistics.  Our app has supported Indonesian major e-commerce with fleets of couriers and massive traffic of orders. Let technology help your business scale by improving efficiency and quality of fulfillment.</h3></span>
                                            </div>
                                            <div class="col-md-1" style="width:1% !important;"></div>
                                            <div class="col-md-3" id="line1" style="background: url({{'images/projects/shttefan.jpg'}})!important;background-size: cover !important;
                                         background-repeat: no-repeat !important; height:210px !important; "> 
                                                <span id="a"><h3>Auction app to simplify and coordinate processes in different locations</h3> </span><span id="b"><h3 class="text">Our app helps companies and banks with many branches engage customers in different locations, merging e-commerce and back-office processes.  Your team easily manages online orders, improving customer satisfaction.</h3></span>
                                            </div>  
                                        </div>
                                    </div>
                                    <br><br>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-3" id="line1" style="background: url({{'images/projects/rawpixel.jpg'}})!important;background-size: cover !important;
                                             background-repeat: no-repeat !important; height:210px !important; ">
                                            <span id="a"><h3>An online platform to help companies manage HR processes and operate efficiently.</h3></span><span id="b"><h3 class="device" style="line-height: 25px; padding-top: 5px;">Companies and organizations benefit from our online platform to efficiently and accurately process all administrative aspects of managing employees. </h3></span>
                                            </div>
                                            <div class="col-md-1" style="width:1% !important;"></div>
                                            <div class="col-md-3" id="line1" style="background: url({{'images/projects/nabeel.jpg'}})!important;background-size: cover !important;
                                             background-repeat: no-repeat !important; height:210px !important;">
                                            <span id="a"><h3>A smart management platform of corporate vehicles, ensuring ease, accuracy, and safety. </h3></span><span id="b"><h3 class="device" style="line-height: 24px;">Managing the use of your company vehicles is key when your team members need to be mobile.  Talk to us about using this digital platform to support your administrative team.</h3></span>
                                            </div>
                                            <div class="col-md-1" style="width:1% !important;"></div>
                                            <div class="col-md-3">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>
    <!-- ENS APPS SCREEN -->
<!-- =========================
     Start FUN FACTS
============================== -->
    <section class="fun_facts parallax" >
        <div class="section_overlay">
            <div class="container wow bounceInLeft" data-wow-duration="1s">
                <div class="row text-center" id="CONTACT">
                
                </div>
            </div>
        </div>
    </section>

    <!-- END FUN FACTS -->

<!-- =========================
     START DOWNLOAD NOW 
============================== -->

        <div class="available_store" >
            <div class="container  wow bounceInRight" data-wow-duration="1s">
                <div class="col-md-6">
                    <div class="available_title">
                        <h2></h2>
                        <p> </p>
                    </div>
                </div>

                    </div>
                </div>
                <!-- END DOWNLOADABLE STORE -->
            </div>
        </div>
    </section>
    <!-- END DOWNLOAD -->

<!-- =========================
     START CONTCT FORM AREA
============================== -->
    <section class="contact page" >
        <div class="section_overlay" style="padding-left:45px;">
            <div class="container">
                <div class="inner_about_title" style="padding-bottom: 30px; height:50px;">
                        <h2  style="text-transform:none !important; text-align:left; font-size:26px;">Work with us</h2>
                        <p> For any general inquiries, please fill in the following contact form:</p>
                    </div>
            </div>
            <br><br>
            <div class="contact_form wow bounceIn">
                <div class="container">

                <!-- START ERROR AND SUCCESS MESSAGE -->
                    <div class="form_error text-center">
                        <div class="name_error hide error">Please Enter your name</div>
                        <div class="email_error hide error">Please Enter your Email</div>
                        <div class="email_val_error hide error">Please Enter a Valid Email Address</div>
                        <div class="message_error hide error">Please Enter Your Message</div>
                    </div>
                    <div class="Sucess"></div>
                <!-- END ERROR AND SUCCESS MESSAGE -->

                <!-- FORM -->    
                    <form action="{{ url('contactus')}}" method="post" role="form">
                        <div class="row">
                            <div class="col-md-4" style="padding-left:15px !important;">
                                <h4>PT Visi Olah Jakarta</h4>
                                <p>Menara BTPN lantai 47</p>
                                <p>Jalan Dr Ide Agung Gde Agung Kav 5.5-5.6 </p>
                                <p>Kawasan Mega Kuningan, Kuningan Timur</p>
                                <p>Setiabudi, Jakarta Selatan 12950</p>
                                <p>Tel/WhatsApp (0812) 1243 3540</p>
                                <p>Email halo@voiceofjakarta.com</p>
                            </div>


                            <div class="col-md-4" style="padding-bottom: 20px;">
                            <br>
                                <input type="text" class="form-control" name="name" placeholder="Name">
                                <input type="email" class="form-control" name="email" placeholder="Email">
                                <input type="text" class="form-control" name="subject" placeholder="Subject">
                                
                            </div>
                            <div class="col-md-4" >
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <br>
                                <textarea style="height:180px;" class="form-control" name="pesan" rows="25" cols="10" placeholder="  Message.."></textarea>
                                <button style="width:110px; float: right; height:40px;" type="submit" class="btn btn-default submit-btn ">SEND</button>

                            </div>
                            
                        </div>
                        <br><br>
                    </form>
                    <!-- END FORM --> 
                </div>
            </div>

            <div class="container">
            </div>
        </div>
    </section>
    <br/>

            </div>
        </div>
    </section>

    <!-- END SUBSCRIPBE FORM -->

<!-- =========================
     FOOTER 
============================== -->

    <section class="copyright">
        <h2></h2>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="copy_right_text">
                    <!-- COPYRIGHT TEXT -->
                        <p>Copyright &copy; PT Visi Olah Jakarta 2017.  All rights reserved.</p>
                    </div>
                </div>

                <!-- <div class="col-md-6">
                    <div class="scroll_top">
                        <a href="#HOME"><i class="fa fa-angle-up"></i></a>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <!-- END FOOTER -->


<!-- =========================
     SCRIPTS 
============================== -->

    
    <script src="{{URL ('asset') }}/js/jquery.min.js "></script>
    <script src="{{URL ('asset') }}/js/bootstrap.min.js "></script>
    <script src="{{URL ('asset') }}/js/owl.carousel.js "></script>
    <script src="{{URL ('asset') }}/js/jquery.fitvids.js "></script>
    <script src="{{URL ('asset') }}/js/smoothscroll.js "></script>
    <script src="{{URL ('asset') }}/js/jquery.parallax-1.1.3.js "></script>
    <script src="{{URL ('asset') }}/js/jquery.prettyPhoto.js "></script>
    <script src="{{URL ('asset') }}/js/jquery.ajaxchimp.min.js "></script>
    <script src="{{URL ('asset') }}/js/jquery.ajaxchimp.langs.js "></script>
    <script src="{{URL ('asset') }}/js/wow.min.js "></script>
    <script src="{{URL ('asset') }}/js/waypoints.min.js "></script>
    <script src="{{URL ('asset') }}/js/jquery.counterup.min.js "></script>
    <script src="{{URL ('asset') }}/js/script.js "></script>



</body>

</html>
