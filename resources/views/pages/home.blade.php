<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>voj</title>
    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:500,600,700,800,900,400,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link  rel="stylesheet" href="{{URL('asset') }}/css/bootstrap.min.css " >
    <!-- Owl Carousel Assets -->
    <link rel="stylesheet"  href="{{URL('asset') }}/css/owl.carousel.css " >
    <link  rel="stylesheet" href="{{URL('asset') }}/css/owl.theme.css " >
    <!-- Pixeden Icon Font -->
    <link rel="stylesheet" href="{{URL('asset') }}/css/Pe-icon-7-stroke.css ">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{URL('asset') }}/css/font-awesome.min.css " >
    <!-- PrettyPhoto -->
    <link  rel="stylesheet" href="{{URL('asset') }}/css/prettyPhoto.css " >
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <!-- Style -->
    <link rel="stylesheet"  href="{{URL('asset') }}/css/style.css " >
    <link  rel="stylesheet" href="{{URL('asset') }}/css/animate.css " >
    <!-- Responsive CSS -->
    <link  rel="stylesheet" href="{{URL('asset') }}/css/responsive.css " >
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        @media screen and (max-width: 580px){
            img.logos{
                height: 70px !important;
                width: auto;
                padding-left: 20px;
            }
        }
    </style>
</head>
<body>
    <!-- PRELOADER -->
    <div class="spn_hol">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
 <!-- END PRELOADER -->

 <!-- =========================
     START ABOUT US SECTION
============================== -->

    <section class="header parallax home-parallax page" id="HOME" style="background: url({{'images/balkonnew.jpg'}})!important;background-size: cover !important;
    background-repeat: no-repeat !important; height:750px !important;">
        <h2></h2>
        <div class="section_overlay">
            <nav class="navbar-default navbar-fixed-top" role="" style="">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- <a class="navbar-brand" href="#"> -->
                        <a class="navbars" href="#">
                            <img class="logos" src="{{URL('images/logofix.png')}}" alt="Logo" class="" style="padding-top:10px;  height:170px ">
                        </a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a style="color:#428bca !important; font-size:18px !important;" href="#HOME">HOME</a> </li>
                            <li><a style="color:#428bca !important; font-size:18px !important;" href="#ABOUT">ABOUT </a> </li>
                            <li><a style="color:#428bca !important; font-size:18px !important;" href="#FEATURES">PROJECT</a></li>
                           <!--  <li><a href="#SCREENS">SCREENS</a> </li>
                           <li><a href="#DOWNLOAD">DOWNLOAD </a> </li> -->
                            <li><a style="color:#428bca !important; font-size:18px !important;" href="#CONTACT">WORK WITH US</a> </li>
                        </ul>
                    </div> 
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container- -->
            </nav>

            <div class="container home-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo text-center">
                                <!-- LOGO -->
                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <div class="home" style="color:white;">
                            <!-- TITLE AND DESC -->
                            <h1 style="font-size:18px;">HOW TO WIN WITH DIGITAL</h1>
                            <p>business and performance</p>
                            <p>management solution for companies</p>
                            <p>and institutions</p>

                            <button type="button" class="btn btn-secondary" style="border: 3px solid; border-color:#f0ad4e; height:35px; width:70px;"> HOW</button>
                            <p></p>
                            <p> Web and Mobile App * platform/system</p>
                            <p> Digital services * media</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-1 col-sm-4">
                        <div class="home-iphone">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- END HEADER SECTION -->
 <!-- =========================
     START ABOUT US SECTION
============================== -->
    <section class="about page" id="ABOUT">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- ABOUT US SECTION TITLE-->
                    <div class="section_title">
                        <h2>About Us</h2>
                        <p>Voice of Jakarta (VOJ) traces its beginnings as an internet-radio broadcast based in New York City which started out in July 2004. In 2009 it reemerged in its home city as a company with a focus on digital technology and communications. 
VOJ’s is a digital and communications consultancy. We provide our clients with digital solution to improve their operations. We help companies build and grow their brand and reputation, and, in a world where influence are passed on via social media, assist them in creating and strengthening their online presence through content creation for the right media and authentic communications with relevant stakeholders. Voice of Jakarta was founded in July 2004 as a free online music streaming service based in New York City, born straight into the age of digital. In 2009 it was incorporated in Jakarta to focus on technology, and in 2016 it’s reconsolidated, embracing both core competencies brought and nurtured by its founders to provide tech and communications solutions that
answers to the challenges of the digital life.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="inner_about_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="about_phone wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
                   
                            <!--<img src="images/jex_top_logo.png" alt="">-->
                        </div>
                   
                    <div class="col-md-6  wow fadeInRight" data-wow-duration="1s" data-wow-delay=".5s">
                        <!-- TITLE -->
                        <div class="inner_about_title">
                            <h2>Our Product</h2>
                            <p></p>
                        <div class="inner_about_desc">

                            <!-- SINGLE DESC -->
                            <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="1s">
                                <!-- ICON -->
                                <div><i class="pe-7s-timer"></i></div>
                                <br/>
                                <!-- HEADING DESCRIPTION -->
                                <h3>M-trak </h3>
                                <p>We live in a fast-changing transitional landscape of media where online marketplace is starting to get a stronghold of the consumers in Indonesia. E-marketplaces rely heavily on logistics, and one of our digital products has supported fleets of couriers and online shop with massive traffic of orders.</p>
                            </div>
                            <!-- END SINGLE DESC -->
                            <!-- SINGLE DESC -->
                            <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="1.5s">
                                <!-- ICON -->
                                <div><i class="pe-7s-target"></i></div>
                                <!-- HEADING DESCRIPTION -->
                                <h3>halosmallbiz </h3>
                                <p>With digital platforms disrupting trade landscape, individual merchants start to claim their place in the marketplace and our solution supports the growth of this online businesses</p>
                            </div>
                            <!-- END SINGLE DESC -->
                            <!-- SINGLE DESC -->
                            <div class="single_about_area fadeInUp wow" data-wow-duration=".5s" data-wow-delay="2s">
                                <!-- ICON -->
                                <div><i class="pe-7s-stopwatch"></i></div>
                                <!-- HEADING DESCRIPTION -->
                                <h3>halorides</h3>
                                <p>Another initiatives brought as at the center of another fast-growing industries, catering to travelers’ need of safe and reliable transportation in urban centers around the country.</p>
                            </div>
                            <!-- END SINGLE DESC -->
                        </div>
                    </div>
                </div>
                 </div>
            </div>
        </div>
<!--
        <div class="video_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 wow fadeInLeftBig">
                    
                        <div class="video_title">
                            <h2>Best App <br>in the market</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                        </div>
                        <div class="video-button">
                            
                            <a class="btn btn-primary btn-video" href="#FEATURES" role="button">Features</a>
                        </div>
                    </div>
                    <div class="col-md-6 wow fadeInRightBig">
                         
                        <div class="video">
                            <img src="images/jex_top_logo.png"  width="250" height="200">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
-->




 <!-- =========================
     START TESTIMONIAL SECTION
============================== -->

    <section id="TESTIMONIAL" class="testimonial parallax">
        <div class="section_overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow bounceInDown">
                        <div id="carousel-example-caption-testimonial" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-caption-testimonial" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-caption-testimonial" data-slide-to="1"></li>
                                <li data-target="#carousel-example-caption-testimonial" data-slide-to="2"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <!-- IMAGE -->
                                                <img src="images/pakbos.jpg" alt="logo" height=50px; width=50px;>
                                                <div class="testimonial_caption">
                                                   <!-- DESCRIPTION -->  
                                                    <h2>Mr. Andy Awidarto</h2>
                                                    <h4><span>SR. Developer,</span> The Code Cafe</h4>
                                                    <p>“Lorem ipsum dolor sit amet, consectetur adipisicing elit, do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.”</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                 <!-- IMAGE -->
                                                <img src="images/alan.jpg" alt="" width=50px; height=40px;>
                                                <div class="testimonial_caption">
                                                <!-- DESCRIPTION --> 
                                                    <h2>Mr. Alan Wahsyahlan</h2>
                                                    <h4><span>SR. Developer,</span> The Code Cafe</h4>
                                                    <p>“Lorem ipsum dolor sit amet, consectetur adipisicing elit, do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.”</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <!-- IMAGE -->
                                                <img src="images/client_3.png" alt="">
                                                <div class="testimonial_caption">
                                                <!-- DESCRIPTION --> 
                                                    <h2>Mr. Rikiplik</h2>
                                                    <h4><span>SR. Developer,</span> The Code Cafe</h4>
                                                    <p>“Lorem ipsum dolor sit amet, consectetur adipisicing elit, do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.”</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- END TESTIMONIAL SECTION -->



<!-- =========================
     START FEATURES
============================== -->
    <section id="FEATURES" class="features page">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- FEATURES SECTION TITLE -->
                    <div class="section_title wow fadeIn" data-wow-duration="1s">
                        <h2>Services</h2>
                        
                    </div>
                    <!-- END FEATURES SECTION TITLE -->
                </div>
            </div>
        </div>
        <div class="feature_inner">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 right_no_padding wow fadeInLeft" data-wow-duration="1s">
                        <!-- FEATURE -->
                        <div class="left_single_feature">
                            <!-- ICON -->
                            <div><span class="pe-7s-monitor"></span></div>
                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3>Digital<span>/</span></h3>
                            <p>Digital Solutions IT services Consultation</p>
                        </div>
                        <!-- END SINGLE FEATURE -->
                        <!-- FEATURE -->
                        <div class="left_single_feature">
                            <!-- ICON -->
                            <div><span class="pe-7s-science"></span></div>
                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3>Comms<span>/</span></h3>
                            <p>Communications Services Strategic Public Relations Digital activation Event</p>
                        </div>
                        <!-- END SINGLE FEATURE -->
                        <!-- FEATURE -->
                        <div class="left_single_feature">
                            <!-- ICON -->
                            <div><span class="pe-7s-phone"></span></div>
                            <!-- FEATURE HEADING AND DESCRIPTION -->
                            <h3>Media<span>/</span></h3>
                            <p>Media Content Production Consultation</p>
                        </div>
                        <!-- END SINGLE FEATURE 
                    </div>
                    <div class="col-md-4">
                        <div class="feature_iphone">
                            
                            <img class="wow bounceIn" data-wow-duration="1s" src="images/iPhone02.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-4 left_no_padding wow fadeInRight" data-wow-duration="1s">

                        <
                        <div class="right_single_feature">
                            -->
<!--                            <div><span class="pe-7s-monitor"></span></div>

                            <
                            <h3><spFEATURE HEADING AND DESCRIPTION -an>/</span>Retina ready</h3>
                            <p>Lorem ipsum dolor, consectetur sed do adipisicing elit, sed do eiusmod tempor incididunt</p>
                        </div>
                        
                        <div class="right_single_feature">
                           
                            <div><span class="pe-7s-phone"></span></div>
                            
                            <h3><span>/</span>Responsive Ready</h3>
                            <p>Lorem ipsum dolor, consectetur sed do adipisicing elit, sed do eiusmod tempor incididunt</p>
                        </div>
                        
                       
                        <div class="right_single_feature">
                           
                            <div><span class="pe-7s-gleam"></span></div>

                            
                            <h3><span>/</span>Clean Code</h3>
                            <p>Lorem ipsum dolor, consectetur sed do adipisicing elit, sed do eiusmod tempor incididunt</p>
                        </div>
                        -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    



<!-- =========================
     START CALL TO ACTION
============================== -->
<!--
    <div class="call_to_action">
        <div class="container">
            <div class="row wow fadeInLeftBig" data-wow-duration="1s">
                <div class="col-md-9">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et olore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                </div>
                <div class="col-md-3">
                    <a class="btn btn-primary btn-action" href="#" role="button">Purchase Now</a>
                </div>
            </div>
        </div>
    </div>
-->
    <!-- END CALL TO ACTION -->


<!-- =========================
     Start APPS SCREEN SECTION
============================== 
    <section class="apps_screen page" id="SCREENS">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 wow fadeInBig" data-wow-duration="1s">
                        
                        <div class="section_title">
                            <h2>Screens</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>                           
                        </div>
                        
                    </div>
                </div>
            </div>
-->
        <div class="screen_slider">
            <div id="demo" class="wow bounceInRight" data-wow-duration="1s">
                <div id="owl-demo" class="owl-carousel">

                    <!-- APPS SCREEN IMAGES 
                    <div class="item">
                    <a href="images/screens/iPhone04.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone04.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                    <div class="item">
                        <a href="images/screens/iPhone05.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone05.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                    <div class="item">
                        <a href="images/screens/iPhone06.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone06.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                    <div class="item">
                        <a href="images/screens/iPhone07.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone07.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                    <div class="item">
                        <a href="images/screens/iPhone08.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone08.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                    <div class="item">
                        <a href="images/screens/iPhone09.png" rel="prettyPhoto[pp_gal]"><img src="images/iPhone09.png" width="60" height="60" alt="APPS SCREEN" /></a>
                    </div>
                    -->
                </div>
            </div>
        </div>
    </section>
    <!-- ENS APPS SCREEN -->
<!-- =========================
     Start FUN FACTS
============================== -->
    <section class="fun_facts parallax">
        <div class="section_overlay">
            <div class="container wow bounceInLeft" data-wow-duration="1s">
                <div class="row text-center">
                <!--
                    <div class="col-md-3">
                        <div class="single_fun_facts">
                            <i class="pe-7s-cloud-download"></i>
                            <h2><span  class="counter_num">699</span> <span>+</span></h2>
                            <p>Downloads</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="single_fun_facts">
                            <i class="pe-7s-look"></i>
                            <h2><span  class="counter_num">1999</span> <span>+</span></h2>
                            <p>Likes</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="single_fun_facts">
                            <i class="pe-7s-comment"></i>
                            <h2><span  class="counter_num">199</span> <span>+</span></h2>
                            <p>Feedbacks</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="single_fun_facts">
                            <i class="pe-7s-cup"></i>
                            <h2><span  class="counter_num">10</span> <span>+</span></h2>
                            <p>Awards</p>
                        </div>
                    </div>
                    -->
                </div>
            </div>
        </div>
    </section>

    <!-- END FUN FACTS -->




<!-- =========================
     START DOWNLOAD NOW 
============================== -->
 <!--   <section class="download page" id="DOWNLOAD">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                   
                    <div class="section_title">
                        <h2>download now</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                    </div>
                  END DOWNLOAD NOW SECTION TITLE 
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="download_screen text-center wow fadeInUp" data-wow-duration="1s">
                        <img src="images/download_screen.png" alt="">
                    </div>
                </div>
            </div>
        </div>
-->
        <div class="available_store">
            <div class="container  wow bounceInRight" data-wow-duration="1s">
                <div class="col-md-6">
                    <div class="available_title">
                        <h2></h2>
                        <p> </p>
                    </div>
                </div>

                <!-- DOWNLOADABLE STORE 
                <div class="col-md-6">
                    <div class="row">
                        <a href="">
                            <div class="col-md-4 no_padding">
                                <div class="single_store">
                                    <i class="fa fa-apple"></i>
                                    <div class="store_inner">
                                        <h2>iOS</h2>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="col-md-4 no_padding">
                            <a href="">
                                <div class="single_store">
                                    <i class="fa fa-android"></i>
                                    <div class="store_inner">
                                        <h2>ANDROID</h2>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-4 no_padding">
                            <a href="">
                                <div class="single_store last">
                                    <i class="fa fa-windows"></i>
                                    <div class="store_inner">
                                        <h2>WINDOWS</h2>
                                    </div>
                                </div>
                            </a>
                        </div>
                        -->
                    </div>
                </div>
                <!-- END DOWNLOADABLE STORE -->
            </div>
        </div>
    </section>
    <!-- END DOWNLOAD -->

<!-- =========================
     START CONTCT FORM AREA
============================== -->
    <section class="contact page" id="CONTACT">
        <div class="section_overlay">
            <div class="container">
                <div class="col-md-10 col-md-offset-1 wow bounceIn">
                    <!-- Start Contact Section Title-->
                    <div class="section_title">
                        <h2>Get Info Voice Of Jakarta </h2>
                        <p></p>
                    </div>
                </div>
            </div>

            <div class="contact_form wow bounceIn">
                <div class="container">

                <!-- START ERROR AND SUCCESS MESSAGE -->
                    <div class="form_error text-center">
                        <div class="name_error hide error">Please Enter your name</div>
                        <div class="email_error hide error">Please Enter your Email</div>
                        <div class="email_val_error hide error">Please Enter a Valid Email Address</div>
                        <div class="message_error hide error">Please Enter Your Message</div>
                    </div>
                    <div class="Sucess"></div>
                <!-- END ERROR AND SUCCESS MESSAGE -->

                <!-- FORM -->    
                    <form action="{{URL::to('contactus')}}" method="post" role="form">
                        <div class="row">
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="name" placeholder="Name">
                                <input type="email" class="form-control" name="email" placeholder="Email">
                                <input type="text" class="form-control" name="subject" placeholder="Subject">
                            </div>


                            <div class="col-md-8">
                                <textarea class="form-control" name="message" rows="25" cols="10" placeholder="  Message Texts..."></textarea>
                                <button type="submit" class="btn btn-default submit-btn ">SEND MESSAGE</button>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM --> 
                </div>
            </div>

            <div class="container">
            <!--
                <div class="row">
                    <div class="col-md-12 wow bounceInLeft">
                        <div class="social_icons">
                            <ul>
                                <li><a href=""><i class="fa fa-facebook"></i></a>
                                </li>
                                <li><a href=""><i class="fa fa-twitter"></i></a>
                                </li>
                                <li><a href=""><i class="fa fa-dribbble"></i></a>
                                </li>
                                <li><a href=""><i class="fa fa-behance"></i></a>
                                </li>
                                <li><a href=""><i class="fa fa-youtube-play"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                -->
            </div>
        </div>
    </section>
    <br/>
    <!-- END CONTACT -->

<!-- =========================
     Start Subscription Form 
============================

   <s') }}ection class="subscribe parallax subscribe-parallax" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
        <div class="section_overlay wow lightSpeedIn">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">

                        
                        <div class="section_title">
                            <h2>SUBSCRIBE US</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                        </div>
                        
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row  wow lightSpeedIn">
                    <div class="col-md-6 col-md-offset-3">
                        
                        <div class="subscription-success"></div>
                        <div class="subscription-error"></div>


                        <form action="{{URL::to('contactus')}}" method="post" id="mc-form" class="subscribe_form">
                            <div class="form-group">
                                
                                <input type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL" placeholder="Enter Email Address">
                            </div>

                                
                            <button type="submit" class="btn btn-default subs-btn">Submit</button>
                        </form>


                    </div>
                </div>
                -->
            </div>
        </div>
    </section>

    <!-- END SUBSCRIPBE FORM -->

<!-- =========================
     FOOTER 
============================== -->

    <section class="copyright">
        <h2></h2>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="copy_right_text">
                    <!-- COPYRIGHT TEXT -->
                        <p>Copyright &copy; 2017 <span>By </span><a href="#">Voice Of Jakarta</a></p>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="scroll_top">
                        <a href="#HOME"><i class="fa fa-angle-up"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END FOOTER -->


<!-- =========================
     SCRIPTS 
============================== -->

    
    <script src="{{URL ('asset') }}/js/jquery.min.js "></script>
    <script src="{{URL ('asset') }}/js/bootstrap.min.js "></script>
    <script src="{{URL ('asset') }}/js/owl.carousel.js "></script>
    <script src="{{URL ('asset') }}/js/jquery.fitvids.js "></script>
    <script src="{{URL ('asset') }}/js/smoothscroll.js "></script>
    <script src="{{URL ('asset') }}/js/jquery.parallax-1.1.3.js "></script>
    <script src="{{URL ('asset') }}/js/jquery.prettyPhoto.js "></script>
    <script src="{{URL ('asset') }}/js/jquery.ajaxchimp.min.js "></script>
    <script src="{{URL ('asset') }}/js/jquery.ajaxchimp.langs.js "></script>
    <script src="{{URL ('asset') }}/js/wow.min.js "></script>
    <script src="{{URL ('asset') }}/js/waypoints.min.js "></script>
    <script src="{{URL ('asset') }}/js/jquery.counterup.min.js "></script>
    <script src="{{URL ('asset') }}/js/script.js "></script>



</body>

</html>
