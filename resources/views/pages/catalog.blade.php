@extends('layout.master')

@section('content')

	<!-- end:fh5co-header -->
	<div class="fh5co-parallax" style="background-image: url(images/slider1.jpg);" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 text-center fh5co-table">
					<div class="fh5co-intro fh5co-table-cell">
						<h1 class="text-center">Supply</h1>
						
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="fh5co-blog-section">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="blog-grid" style="background-image: url('../luxe/images/1.png');">
						<div class="date text-center">
							<!--<span>09</span>
							<small>Aug</small> -->
						</div>
					</div>
					<div class="desc">
						<h3><a href="#">Stationary office</a></h3>
					</div>
				</div>
				<div class="col-md-4">
					<div class="blog-grid" style="background-image: url('../luxe/images/2.png');">
						<div class="date text-center">
							<!--<span>09</span>
							<small>Aug</small> -->
						</div>
					</div>
					<div class="desc">
						<h3><a href="#">Bolt & Nut</a></h3>
					</div>
				</div>
				<div class="col-md-4">
					<div class="blog-grid" style="background-image: url('../luxe/images/3.png');">
						<div class="date text-center">
							<!--<span>09</span>
							<small>Aug</small> -->
						</div>
					</div>
					<div class="desc">
						<h3><a href="#">Mechanical Electrical </a></h3>
					</div>
				</div>
				<div class="col-md-4">
					<div class="blog-grid" style="background-image: url('../luxe/images/4.png');">
						<div class="date text-center">
							<!--<span>09</span>
							<small>Aug</small> -->
						</div>
					</div>
					<div class="desc">
						<h3><a href="#">Pipe & Valves</a></h3>
					</div>
				</div>
				<div class="col-md-4">
					<div class="blog-grid" style="background-image: url('../luxe/images/5.png');">
						<div class="date text-center">
							<!--<span>09</span>
							<small>Aug</small> -->
						</div>
					</div>
					<div class="desc">
						<h3><a href="#">Electrical</a></h3>
					</div>
				</div>
				<div class="col-md-4">
					<div class="blog-grid" style="background-image: url('../luxe/images/6.png');">
						<div class="date text-center">
							<!--<span>09</span>
							<small>Aug</small> -->
						</div>
					</div>
					<div class="desc">
						<h3><a href="#">Safety Equipment</a></h3>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop