@extends('beautymail::templates.sunny')

@section('content')

	@include('beautymail::templates.widgets.newfeatureStart')

		<h3 class="secondary"><strong>{{ $subject}}</strong></h3>
		<p>Pengirim : {{ $name}}, {{ $email }}</p>
		<br>
		<p>{{ $pesan}}</p>
		
	@include('beautymail::templates.widgets.newfeatureEnd')

@stop