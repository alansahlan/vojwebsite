<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Talent</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{url('limitless')}}/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="{{url('limitless')}}/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="{{url('limitless')}}/assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="{{url('limitless')}}/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="{{url('limitless')}}/assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/loaders/blockui.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/ui/nicescroll.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/ui/drilldown.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/ui/fab.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/forms/wizards/steps.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/core/libraries/jasny_bootstrap.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/forms/validation/validate.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/extensions/cookie.js"></script>

	<script type="text/javascript" src="{{url('limitless')}}/assets/js/core/app.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/pages/wizard_steps.js"></script>

	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/ui/ripple.min.js"></script>
	<!-- /theme JS files -->

</head>

<body class="navbar-bottom">

	<!-- Page header -->
	<div class="page-header page-header-inverse bg-indigo">

		<!-- Main navbar -->
		

		<!-- Page header content -->
		<div class="page-header-content">
			<div class="page-title">
				<h4>Talent - Steps</h4>
			</div>

			<div class="heading-elements">
				<ul class="breadcrumb heading-text">
					<li><a href="{{url('/')}}"><i class="icon-home2 position-left"></i> Home</a></li>
					<li>Talent - Steps</li>
				</ul>
			</div>
		</div>
		<!-- /page header content -->


		<!-- Second navbar -->

	</div>
	<!-- /page header -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Basic setup -->
	           
	            <!-- /basic setup -->


	            <!-- Wizard with validation -->
	            <div class="panel panel-white">
					<div class="panel-heading">
						<h6 class="panel-title">Form Registrasi</h6>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>

                	{!! Form::open (['url' => 'recruit', 'class' => 'steps-validation']) !!}
						<h6>Personal data</h6>
						<fieldset>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Nama lengkap: <span class="text-danger">*</span></label>
										<input type="text" name="fullname" class="form-control required" placeholder="Nama lengkap">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Alamat sesuai KTP : <span class="text-danger">*</span></label>
										<input type="text" name="adress_ktp" class="form-control required" placeholder="alamat">
									</div>
								</div>
							</div>
									
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Nama panggilan : <span class="text-danger">*</span></label>
										<input type="text" name="nickname" class="form-control required" placeholder="Nama panggilan">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label>Alamat tempat tinggal saat ini (apabila berbeda dengan KTP): </label>
										<input type="text" name="adress_now" class="form-control " placeholder="Alamat tempat tinggal">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
								
									<div class="form-group">
										<label>Tempat lahir : <span class="text-danger">*</span></label>
										<input type="text" name="residence" class="form-control required" placeholder="Tempat lahir">
									</div>
									<label>Tanggal lahir:</label>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<select name="birth_month" data-placeholder="Month" class="select">
													<option></option>
													<option value="january">January</option>
													<option value="february">February</option>
													<option value="march">March</option>
													<option value="april">April</option>
													<option value="may">May</option>
													<option value="june">June</option>
													<option value="jully">July</option>
													<option value="august">August</option>
													<option value="september">September</option>
													<option value="october">October</option>
													<option value="november">November</option>
													<option value="december">December</option>
												</select>
											</div>
										</div>

										<div class="col-md-4">
											<div class="form-group">
												<select name="birth_day" data-placeholder="Day" class="select">
													<option></option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
													<option value="6">6</option>
													<option value="7">7</option>
													<option value="8">8</option>
													<option value="9">9</option>
													<option value="10">10</option>
													<option value="11">11</option>
													<option value="12">12</option>
													<option value="13">13</option>
													<option value="14">14</option>
													<option value="15">15</option>
													<option value="16">16</option>
													<option value="17">17</option>
													<option value="18">18</option>
													<option value="19">19</option>
													<option value="20">20</option>
													<option value="21">21</option>
													<option value="22">22</option>
													<option value="23">23</option>
													<option value="24">24</option>
													<option value="25">25</option>
													<option value="26">26</option>
													<option value="27">27</option>
													<option value="28">28</option>
													<option value="29">29</option>
													<option value="30">30</option>
													<option value="31">31</option>
												</select>
											</div>
										</div>

										<div class="col-md-4">
											<div class="form-group">
												<select name="birth_year" data-placeholder="Year" class="select">
													<option></option>
													<option value="1980">1980</option>
													<option value="1981">1981</option>
													<option value="1982">1982</option>
													<option value="1983">1983</option>
													<option value="1984">1984</option>
													<option value="1985">1985</option>
													<option value="1986">1986</option>
													<option value="1987">1987</option>
													<option value="1988">1988</option>
													<option value="1989">1989</option>
													<option value="1990">1990</option>
													<option value="1991">1991</option>
													<option value="1992">1992</option>
													<option value="1993">1993</option>
													<option value="1994">1994</option>
													<option value="1995">1995</option>
													<option value="1996">1996</option>
													<option value="1997">1997</option>
													<option value="1998">1998</option>
													<option value="1999">1999</option>
													<option value="2000">2000</option>
													<option value="2001">2001</option>
													<option value="2002">2002</option>
													<option value="2003">2003</option>
												</select>
											</div>
										</div>
									</div>
									<!--
									<div class="form-group">
										<label>Usia : </label>
										<input type="text" name="age" class="form-control" placeholder="Usia">
									</div>
									-->
									<div class="form-group">
										<label>Nomor yang bisa dihubungi:<span class="text-danger">*</span></label>
										<input type="text" name="phone1" class="form-control required" placeholder="phone 1" >
										<input type="text" name="phone2" class="form-control" placeholder="phone 2" >
									</div>
									<div class="form-group">
										<label>Email :<span class="text-danger">*</span></label>
										<input type="email" name="email" class="form-control required" placeholder="Your Email" >
										</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
									<label>Pada saat ini menggunakan device (boleh pilih lebih dari satu) : <span class="text-danger">*</span></label>
									<select name="device[]" data-placeholder="pilih device" class="select" multiple>
													<option></option>
													<option name="smartphone" value="smartphone">Smartphone</option>
													<option name="tablet" value="tablet">Tablet</option>
													<option name="dekstop" value="Dekstop komputer">Desktop Komputer</option>
													<option name="laptop" value="Laptop/notebook">Laptop / Notebook</option>
									</select>
								</div>
									<div class="form-group">
										<label>Media sosial yang digunakan (boleh pilih lebih dari satu):</label>
												<select name="medsos[]" data-placeholder="pilih media sosial" class="select" multiple>
													<option></option>
													<option name="facebook" value="Facebook">Facebook</option>
													<option name="instagram" value="Instagram">Instagram</option>
													<option name="twitter" value="Twitter">Twitter</option>
													<option name="linkdin" value="LinkdIn">LinkdIn</option>
													<option name="path" value="Path">Path</option>
												</select>
												<label>Lainnya (sebutkan):</label>
 										<br>
		                                <input type="text" name="other_medsos" placeholder="Lainnya" class="form-control ">
									</div>
									<div class="form-group">
										<label>Aplikasi chat yang digunakan (boleh pilih lebih dari satu):</label>
												<select name="appchat[]" data-placeholder="pilih aplikasi" class="select" multiple>
													<option></option>
													<option name="whatsapp" value="WhatsApp">WhatsApp</option>
													<option name="messenger" value="Messenger">Messenger</option>
													<option name="bbm" value="BBM">BBM</option>
													<option name="line" value="Line">Line</option>
													<option name="telegram" value="Telegram">Telegram</option>
												</select>
												<label>Lainnya (sebutkan):</label>
 										<br>
		                                <input type="text" name="other_appchat" placeholder="Lainnya" class="form-control ">
									</div>
								</div>
							</div>


							<div class="row">
								

								<div class="col-md-6">
									
								</div>
							</div>
						</fieldset>

						<h6>Education</h6>
						<fieldset>
						<div class="row">
						<div class="col-md-6">
							
						<label>Pendidikan: <span class="text-danger">*</span></label>
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<select class="select" name="education" data-placeholder="Pendidikan Terakhir">
											<option></option>
											<option value="smk">SMK</option>
											<option value="sma">SMA</option>
											<option value="d3">D3</option>
											<option value="s1">S1</option>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<select class="select" name="status_education" data-placeholder="pilih status">
											<option></option>
											<option value="lulus">Lulus</option>
											<option value="belum lulus">Tidak / Belum lulus</option>
										</select>
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<input type="text" name="name_scholl" placeholder="Nama Sekolah/Perguruan Tinggi" class="form-control required">
									</div>
								</div>
								
							</div>
							<div class="form-group">
										<input type="text" name="program_study" placeholder="Program/Bidang Studi" class="form-control required">
									</div>
							<label>Pendidikan lainnya (jika ada) :</label>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="other_education1" placeholder="Nama Sekolah / Instansi" class="form-control">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<select class="select" name="status1" data-placeholder="pilih status">
											<option></option>
											<option value="lulus">Lulus</option>
											<option value="belum lulus">Tidak / Belum lulus</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="other_education2" placeholder="Nama Sekolah / Instansi" class="form-control ">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<select class="select" name="status2" data-placeholder="pilih status">
											<option></option>
											<option value="lulus">Lulus</option>
											<option value="belum lulus">Tidak / Belum lulus</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="other_education3" placeholder="Nama Sekolah / Instansi" class="form-control ">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<select class="select" name="status3" data-placeholder="pilih status">
											<option></option>
											<option value="lulus">Lulus</option>
											<option value="belum lulus">Tidak / Belum lulus</option>
										</select>
									</div>
								</div>
							</div>
							</div>


							<div class="col-md-6">
								
									<div class="form-group">
										<label>Pernah Mendukung(menjadi panitia) Kegiatan apa saja di tingkat SMA & Perguruan tinggi: </label>
		                                <input type="text" name="degree_level1" placeholder="kegiatan 1" class="form-control">
		                                <input type="text" name="degree_level2" placeholder="kegiatan 2" class="form-control">
		                                <input type="text" name="degree_level3" placeholder="kegiatan 3" class="form-control">
		                                <input type="text" name="degree_level4" placeholder="kegiatan 4" class="form-control ">
	                                </div>
							</div>
							<div class="col-md-6">
									<label>Bahasa yang dikuasai :</label>
									<div class="row">
									<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="language1" placeholder="language1" class="form-control" value="Bahasa Indonesia">
									</div>
									</div>
								<div class="col-md-4">
									<div class="form-group">
	                                    <select name="language_skill1" data-placeholder="Pilih skill" class="select">
	                                        <option></option>
	                                        <option value="fasih">Fasih</option> 
	                                        <option value="menguasai pasif">Menguasai pasif</option> 
	                                        <option value="tahu sedikit-sedikit">Tahu sedikit-sedikit</option>                                
	                                    </select>
                                    </div>
								</div>
								</div>	
								</div>
								<div class="col-md-6">
									<div class="row">
									<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="language2" placeholder="language2" class="form-control" value="Bahasa Inggris">
									</div>
									</div>
								<div class="col-md-4">
									<div class="form-group">
	                                    <select name="language_skill2" data-placeholder="Pilih skill" class="select">
	                                        <option></option>
	                                        <option value="fasih">Fasih</option> 
	                                        <option value="menguasai pasif">Menguasai pasif</option> 
	                                        <option value="tahu sedikit-sedikit">Tahu sedikit-sedikit</option>                                
	                                    </select>
                                    </div>
								</div>
								</div>	
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Ekstrakurikuler yang diikuti di sekolah (tingkat SMA dan Perguruan Tinggi) : </label>
		                                <input type="text" name="extra_kurikuler" placeholder="Ekstrakurikuler" class="form-control ">
	                                </div>
								</div>

							
							</div>
						</fieldset>

						<h6>Technical Skills</h6>
						<fieldset>

							<div class="row">
								<div class="col-md-6">
									<label> 1. Sistem Operasi yang pernah digunakan:</label>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="sistem_operasi1" data-placeholder="Pilih sistem operasi" class="select">
	                                        <option></option>
	                                        <option value="Microsoft Windows">Microsoft Windows</option> 
	                                        <option value="Linux">Linux</option> 
	                                        <option value="Unix">Unix</option>    
	                                        <option value="Apple Ios Osx/macOs">Apple Ios Osx/macOs</option> 
	                                        <option value="IBM AS 400">IBM AS 400</option>                     </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="os_version1" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="os_skill1" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="sistem_operasi2" data-placeholder="Pilih sistem operasi" class="select">
	                                        <option></option>
	                                        <option value="Microsoft Windows">Microsoft Windows</option> 
	                                        <option value="Linux">Linux</option> 
	                                        <option value="Unix">Unix</option>    
	                                        <option value="Apple Ios Osx/macOs">Apple Ios Osx/macOs</option> 
	                                        <option value="IBM AS 400">IBM AS 400</option>                     </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="os_version2" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="os_skill2" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>																		
									<label>Lainnya (sebutkan): </label>
 										<br>
		                                <input type="text" name="os_other" placeholder="Lainnya ..." class="form-control">
									<br>
		                                <label>2. Aplikasi Database yang pernah digunakan:</label>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_database1" data-placeholder="Pilih Aplikasi Database" class="select">
	                                        <option></option>
	                                        <option value="Microsoft Access">Microsoft Access</option> 
	                                        <option value="Microsoft SQL">Microsoft SQL</option> 
	                                        <option value="Oracle">Oracle</option>    
	                                        <option value="DB2">DB2</option> 
	                                        <option value="Kohezion">Kohezion</option>                     </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="db_version1" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="db_skill1" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_database2" data-placeholder="Pilih Aplikasi Database" class="select">
	                                        <option></option>
	                                        <option value="Microsoft Access">Microsoft Access</option> 
	                                        <option value="Microsoft SQL">Microsoft SQL</option> 
	                                        <option value="Oracle">Oracle</option>    
	                                        <option value="DB2">DB2</option> 
	                                        <option value="Kohezion">Kohezion</option>                     </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="db_version2" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="db_skill2" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									<label>Lainnya (sebutkan): </label>
 										<br>
		                                <input type="text" name="db_other" placeholder="Lainnya ..." class="form-control">
		                                <br>
		                                <label>3. Aplikasi Microsoft Office yang pernah digunakan:</label>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_ms_office1" data-placeholder="Pilih Aplikasi Ms-Office" class="select">
	                                        <option></option>
	                                        <option value="Microsoft Word">Microsoft Word</option> 
	                                        <option value="Microsoft Excel">Microsoft Excel</option> 
	                                        <option value="Microsoft Power Point">Microsoft Power Point</option>
	                                        <option value="Microsoft Access">Microsoft Access</option> 
	                                        <option value="Microsoft Outlook">Microsoft Outlook</option> 
	                                         </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="office_version1" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="office_skill1" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_ms_office2" data-placeholder="Pilih Aplikasi Ms-Office" class="select">
	                                        <option></option>
	                                        <option value="Microsoft Word">Microsoft Word</option> 
	                                        <option value="Microsoft Excel">Microsoft Excel</option> 
	                                        <option value="Microsoft Power Point">Microsoft Power Point</option>
	                                        <option value="Microsoft Access">Microsoft Access</option> 
	                                        <option value="Microsoft Outlook">Microsoft Outlook</option> 
	                                         </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="office_version2" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="office_skill2" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_ms_office3" data-placeholder="Pilih Aplikasi Ms-Office" class="select">
	                                        <option></option>
	                                        <option value="Microsoft Word">Microsoft Word</option> 
	                                        <option value="Microsoft Excel">Microsoft Excel</option> 
	                                        <option value="Microsoft Power Point">Microsoft Power Point</option>
	                                        <option value="Microsoft Access">Microsoft Access</option> 
	                                        <option value="Microsoft Outlook">Microsoft Outlook</option> 
	                                         </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="office_version3" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="office_skill3" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_ms_office4" data-placeholder="Pilih Aplikasi Ms-Office" class="select">
	                                        <option></option>
	                                        <option value="Microsoft Word">Microsoft Word</option> 
	                                        <option value="Microsoft Excel">Microsoft Excel</option> 
	                                        <option value="Microsoft Power Point">Microsoft Power Point</option>
	                                        <option value="Microsoft Access">Microsoft Access</option> 
	                                        <option value="Microsoft Outlook">Microsoft Outlook</option> 
	                                         </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="office_version4" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="office_skill4" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>										
									<label>Lainnya (sebutkan): </label>
 										<br>
		                                <input type="text" name="office_other" placeholder="Lainnya ..." class="form-control">
		                                <br>
		                                 <label>4. Aplikasi pengolahan gambar yang pernah digunakan:</label>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_gambar1" data-placeholder="Pilih Aplikasi" class="select">
	                                        <option></option>
	                                        <option value="Adobe Photoshop">Adobe Photoshop</option> 
	                                        <option value="Adobe illustrator">Adobe Illustrator</option> 
	                                        <option value="Corel PaintShop Pro">Corel PaintShop Pro</option>
	                                        <option value="Cyberlink PhotoDirector">Cyberlink PhotoDirector</option> 
	                                        <option value="Corel Draw">Corel Draw</option> 
	                                         </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="img_version1" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="img_skill2" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_gambar2" data-placeholder="Pilih Aplikasi" class="select">
	                                        <option></option>
	                                        <option value="Adobe Photoshop">Adobe Photoshop</option> 
	                                        <option value="Adobe illustrator">Adobe illustrator</option> 
	                                        <option value="Corel PaintShop Pro">Corel PaintShop Pro</option>
	                                        <option value="Cyberlink PhotoDirector">Cyberlink PhotoDirector</option> 
	                                        <option value="Corel Draw">Corel Draw</option> 
	                                         </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="img_version2" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="img_skill2" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									<label>Lainnya (sebutkan): </label>
 										<br>
		                                <input type="text" name="other_img" placeholder="Lainnya ..." class="form-control">
		                                <br>
		                                 <label>5. Aplikasi antivirus yang pernah digunakan:</label>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_antivirus1" data-placeholder="Pilih Aplikasi" class="select">
	                                        <option></option>
	                                        <option value="AVG">AVG</option> 
	                                        <option value="McAfee">McAfee</option> 
	                                        <option value="BitDefender">BitDefender</option>
	                                        <option value="Cymantec Norton">Cymantec Norton</option> 
	                                        <option value="Kaspersky">Kaspersky</option> 
	                                         </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="virus_version1" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="virus_skill1" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_antivirus2" data-placeholder="Pilih Aplikasi" class="select">
	                                        <option></option>
	                                        <option value="AVG">AVG</option> 
	                                        <option value="McAfee">McAfee</option> 
	                                        <option value="BitDefender">BitDefender</option>
	                                        <option value="Cymantec Norton">Cymantec Norton</option> 
	                                        <option value="Kaspersky">Kaspersky</option> 
	                                         </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="virus_version2" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="virus_skill2" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									<label>Lainnya (sebutkan): </label>
 										<br>
		                                <input type="text" name="other_virus" placeholder="Lainnya ..." class="form-control">
		                                <br>
		                                 <label>6. Aplikasi Firewall yang pernah digunakan:</label>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_firewall1" data-placeholder="Pilih Aplikasi" class="select">
	                                        <option></option>
	                                        <option value="ZoneAlarm">ZoneAlarm</option> 
	                                        <option value="Comodo">Comodo</option>
	                                        <option value="McAfee">McAfee</option>  
	                                        <option value="Avg">Avg</option> 
	                                        <option value="Symantec">Symantec</option> 
	                                         </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="firewall_version1" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="firewall_skill1" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_firewall2" data-placeholder="Pilih Aplikasi" class="select">
	                                        <option></option>
	                                        <option value="ZoneAlarm">ZoneAlarm</option> 
	                                        <option value="Comodo">Comodo</option>
	                                        <option value="McAfee">McAfee</option>  
	                                        <option value="Avg">Avg</option> 
	                                        <option value="Symantec">Symantec</option> 
	                                         </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="firewall_version2" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="firewall_skill2" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>

									<label>Lainnya (sebutkan): </label>
 										<br>
		                                <input type="text" name="other_fire" placeholder="Lainnya ..." class="form-control">
		                                <br>
		                                 <label>7 Aplikasi Video player yang pernah digunakan?</label>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_video1" data-placeholder="Pilih Aplikasi" class="select">
	                                        <option></option>
	                                        <option value="VLC Media Player">VLC Media Player</option> 
	                                        <option value="KM Player">KM Player</option>
	                                        <option value="DivX Player">DivX Player</option>  
	                                        <option value="Real Player">Real Player</option> 
	                                        <option value="Windows Media player">Windows Media player</option> 
	                                        </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="video_version1" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="video_skill1" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_video2" data-placeholder="Pilih Aplikasi" class="select">
	                                        <option></option>
	                                        <option value="VLC Media Player">VLC Media Player</option> 
	                                        <option value="KM Player">KM Player</option>
	                                        <option value="DivX Player">DivX Player</option>  
	                                        <option value="Real Player">Real Player</option> 
	                                        <option value="Windows Media player">Windows Media player</option> 
	                                        </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="video_version2" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="video_skill2" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									<label>Lainnya (sebutkan): </label>
 										<br>
		                                <input type="text" name="other_video" placeholder="Lainnya ..." class="form-control">
		                                <br>	
								 <label>8. Aplikasi Audio player yang pernah digunakan?</label>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_Audio1" data-placeholder="Pilih Aplikasi" class="select">
	                                        <option></option>
	                                        <option value="MusicBee">MusicBee</option> 
	                                        <option value="foobar2000">foobar2000</option>
	                                        <option value="Winamp">Winamp</option>  
	                                        <option value="Media Monkey">Media Monkey</option> 
	                                        <option value="AIMP">AIMP</option> 
	                                        </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="audio_version1" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="audio_skill1" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_Audio2" data-placeholder="Pilih Aplikasi" class="select">
	                                        <option></option>
	                                        <option value="MusicBee">MusicBee</option> 
	                                        <option value="foobar2000">foobar2000</option>
	                                        <option value="Winamp">Winamp</option>  
	                                        <option value="Media Monkey">Media Monkey</option> 
	                                        <option value="AIMP">AIMP</option> 
	                                        </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="audio_version2" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="audio_skill2" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
								
									<label>Lainnya (sebutkan): </label>
 										<br>
		                                <input type="text" name="other_audio" placeholder="Lainnya ..." class="form-control">
		                                <br>

		                                 <label>9. Aplikasi tools yang pernah digunakan?</label>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_Tools1" data-placeholder="Pilih Aplikasi" class="select">
	                                        <option></option>
	                                        <option value="Adobe Acrobat">Adobe Acrobat</option> 
	                                        <option value="Microsoft Visio">Microsoft Visio</option>
	                                        <option value="Winzip">Winzip</option>  
	                                        <option value="CCleaner ">CCleaner </option> 
	                                        <option value="BlueStack">BlueStack</option> 
	                                        </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="tiils_version1" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="tiils_skill2" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_Tools2" data-placeholder="Pilih Aplikasi" class="select">
	                                        <option></option>
	                                        <option value="Adobe Acrobat">Adobe Acrobat</option> 
	                                        <option value="Microsoft Visio">Microsoft Visio</option>
	                                        <option value="Winzip">Winzip</option>  
	                                        <option value="CCleaner ">CCleaner </option> 
	                                        <option value="BlueStack">BlueStack</option> 
	                                        </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="tiils_version2" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="tiils_skill2" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
								
									<label>Lainnya (sebutkan): </label>
 										<br>
		                                <input type="text" name="other_tools" placeholder="Lainnya ..." class="form-control">
		                                <br>

		                                <label>10. Aplikasi internet browser yang pernah digunakan?</label>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_Internet1" data-placeholder="Pilih Aplikasi" class="select">
	                                        <option></option>
	                                        <option value="Internet Explorer">Internet Explorer</option> 
	                                        <option value="Mozila Firefox">Mozila Firefox</option>
	                                        <option value="Google chrome">Google chrome</option>  
	                                        <option value=" Sapari">Sapari</option> 
	                                        <option value="Opera">Opera</option> 
	                                        </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="internet_version1" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="internet_skill1" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									
									
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="aplikasi_Internet2" data-placeholder="Pilih Aplikasi" class="select">
	                                        <option></option>
	                                        <option value="Internet Explorer">Internet Explorer</option> 
	                                        <option value="Mozila Firefox">Mozila Firefox</option>
	                                        <option value="Google chrome">Google chrome</option>  
	                                        <option value=" Sapari">Sapari</option> 
	                                        <option value="Opera">Opera</option> 
	                                        </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="internet_version2" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="internet_skill2" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
								
									<label>Lainnya (sebutkan): </label>
 										<br>
		                                <input type="text" name="other_brows" placeholder="Lainnya ..." class="form-control">
		                                <br>

		                                <label>11.Bahasa Program yang pernah digunakan?</label>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="bahasa_program1" data-placeholder="Pilih Aplikasi" class="select">
	                                        <option></option>
	                                        <option value="Bahasa C">Bahasa C</option> 
	                                        <option value="JAVA">JAVA</option>
	                                        <option value="PHP">PHP</option>  
	                                        <option value=" SQL">SQL</option> 
	                                        <option value="Phyton">Phyton</option> 
	                                        </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="program_version1" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="program_skill1" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
											<select name="bahasa_program2" data-placeholder="Pilih Aplikasi" class="select">
	                                        <option></option>
	                                        <option value="Bahasa C">Bahasa C</option> 
	                                        <option value="JAVA">JAVA</option>
	                                        <option value="PHP">PHP</option>  
	                                        <option value=" SQL">SQL</option> 
	                                        <option value="Phyton">Phyton</option> 
	                                        </select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<input type="text" name="program_version2" placeholder="Versi(terakhir saja)" class="form-control">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
											<select class="select" name="program_skill2" data-placeholder="Tingkat pengetahuan">
											<option></option>
											<option value="cukup">Cukup</option>
											<option value="baik">Baik</option>
											<option value="expert/ahli">Expert/Ahli</option>
											</select>
											</div>
										</div>
									</div>
									
	
									<label>Lainnya (sebutkan): </label>
 										<br>
		                                <input type="text" name="other_program" placeholder="Lainnya ..." class="form-control">
		                                <br>
		             	
		             	</fieldset>
		             {!! Form::close() !!}                    
	            <!-- /remote content source -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<!-- Footer -->
	<div class="navbar navbar-default navbar-fixed-bottom footer">
		<ul class="nav navbar-nav visible-xs-block">
			<li><a class="text-center collapsed" data-toggle="collapse" data-target="#footer"><i class="icon-circle-up2"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="footer">
			<div class="navbar-text">
				&copy; 2017. <a href="#" class="navbar-link">voj</a>  <a href="http://voiceofjakarta.id" class="navbar-link" target="_blank"></a>
			</div>

			<div class="navbar-right">
				<ul class="nav navbar-nav">
					<li><a href="#">About</a></li>
					<li><a href="#">Terms</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /footer -->

</body>
</html>
