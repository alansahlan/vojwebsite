<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Talent</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{url('limitless')}}/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="{{url('limitless')}}/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="{{url('limitless')}}/assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="{{url('limitless')}}/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="{{url('limitless')}}/assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/loaders/blockui.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/ui/nicescroll.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/ui/drilldown.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/ui/fab.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/forms/wizards/steps.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/core/libraries/jasny_bootstrap.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/forms/validation/validate.min.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/extensions/cookie.js"></script>

	<script type="text/javascript" src="{{url('limitless')}}/assets/js/core/app.js"></script>
	<script type="text/javascript" src="{{url('limitless')}}/assets/js/pages/wizard_steps.js"></script>

	<script type="text/javascript" src="{{url('limitless')}}/assets/js/plugins/ui/ripple.min.js"></script>
	<!-- /theme JS files -->

</head>

<body class="navbar-bottom">
<div class="container">

<h4>Data Anda Berhasil di Save</h4>
</div>
<div class="navbar navbar-default navbar-fixed-bottom footer">
		<ul class="nav navbar-nav visible-xs-block">
			<li><a class="text-center collapsed" data-toggle="collapse" data-target="#footer"><i class="icon-circle-up2"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="footer">
			<div class="navbar-text">
				&copy; 2017. <a href="#" class="navbar-link">voj</a>  <a href="http://voiceofjakarta.id" class="navbar-link" target="_blank"></a>
			</div>

			<div class="navbar-right">
				<ul class="nav navbar-nav">
					<li><a href="#">About</a></li>
					<li><a href="#">Terms</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /footer -->

</body>
</html>